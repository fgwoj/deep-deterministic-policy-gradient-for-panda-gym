# Deep Deterministic Policy Gradient for Panda-Gym

## Introduction
In this project, I have chosen a Deep Determinis3c Policy Gradient (DDPG) which is common reinforcement learning algorithm that is produced to learn continuous control policies for 

## Architecture 
DDPG is an actor-critic algorithm that builds upon the DQN and policy gradient methods. It uses two neural networks, the actor and critic networks, to learn a deterministic policy and evaluate its quality, respectively. “It concurrently learns a Q-function and a policy. It uses off-policy data and the Bellman equa3on to learn the Q-function, and uses the Q-function to learn the policy. It means that it is also an extension of the existing DQN algorithm that we had in labs.” (spinningup.openai.com, n.d.) “The actor network for DDPG is trained to maximize the es3mated value of the selected ac3ons, while the cri3c network is trained to minimize the difference between the es3mated value and the actual value of the state-ac3on pairs.” (Wu et al., 2019)

In conclusion, I think DDPG as it is a popular algorithm is still well suited for my problem due to the ability to handle high-dimensional continuous action spaces which as I have mentioned above, it allows for relatively stable and easy implementation despite its own disadvantages

## Architecture 
The main advantage of DDPG is that it able to handle high-dimensional continuous action spaces. This allows to be used for a variety of different real-world applica3ons in robotics, autonomous vehicles, games where con3nuous control is essential part of these systems.

The next positive of DDPG is that it is stable and easy to implement comparing to other algorithms such as HER or other policy gradient methods. DDPG uses a determinist policy that is less sensi3ve to exploration and converge faster. It is also using reply buffer to store experience, so it will update the actor and cri3c network asynchronously. This feature may help in stabilization of the learning process

The biggest drawback of DDPG is that it requires lots of computa3on as the result it is expensive and requires large amounts of memory to store the replay buffer. Additionally, DDPG can be sensi3ve to the choice of hyperparameters, such as the learning rate and batch size, and may require significant tuning to achieve good performance.

## Performance
In terms of performance, according to paper “panda-gym: Open-source goal-conditioned environments for robotic learning” the number of timesteps required to resolve a task (in my case DDPG) allows to reach the success rate for about “50% for the pick and place task” (Gallouédec et al., n.d.) that I have completed during this project.

In practice using stable_baselines3 implementation of DDPG. The highest success rate I have reached was about 25% which is not satisfaction for this kind of task.

## Getting started
Download ipynb 

## References
Gallouédec, Q., Cazin, N., Dellandréa, E. and Chen, L. (n.d.). panda-gym: Open-source goal- conditioned environments for robotic learning. [online] Available at: https://arxiv.org/pdf/2106.13687.pdf

spinningup.openai.com. (n.d.). Deep Deterministic Policy Gradient — Spinning Up documentation. [online] Available at: https://spinningup.openai.com/en/latest/algorithms/ddpg.html#:~:text=Deep%20Deterministic%20Policy%20Gradient%20(DDPG

Wu, M., Gao, Y., Jung, A., Zhang, Q. and Du, S. (2019). The Actor-Dueling-Critic Method for Reinforcement Learning. Sensors, 19(7), p.1547. doi:https://doi.org/10.3390/s19071547.
